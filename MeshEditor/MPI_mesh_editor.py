"""
This file shows, how to  build change mesh in parallel.
The mesh construction is done on rank 0, then redistributed on 
main communicator (comm).
"""
from dolfin import * 
import numpy as np
import matplotlib.pyplot as plt
import time as tm
comm = MPI.comm_world
rank = comm.Get_rank()

if rank ==0:
    colour = 1
else:
    colour = 0
local_comm = comm.Split(colour)
local_rank = local_comm.Get_rank()

# rebuilding mesh using MeshEditor
def dolfin_mesh(coords,cls,local_comm,global_comm,colour):
    if colour==1:
        editor = MeshEditor()

        n_coords = coords.shape[0] 
        n_cls = cls.shape[0]
    
        mesh = Mesh(local_comm)
        editor.open(mesh,'triangle', 2, 2)  # top. and geom. dimension are both 2
        editor.init_vertices(n_coords )  # number of vertices
        editor.init_cells(n_cls)     # number of cells
        for i in range(n_coords):
            editor.add_vertex(i, coords[i])
        for i in range(n_cls):
            editor.add_cell(i, cls[i])
        editor.close()
    if colour ==1:
        File(local_comm,'mesh.xml') << mesh
    global_comm.Barrier()
    mesh = Mesh(global_comm,'mesh.xml')
    return mesh

mesh = UnitSquareMesh( local_comm,50,50,'crossed',)

#get mesh specifications
cells = mesh.cells()
coors = mesh.coordinates()
if colour ==1:
    coors = coors**2
#from distributed_mesh import build_dolfin_mesh
toc = tm.time()
#mesh = build_dolfin_mesh(coors,cells,rank)
mesh = dolfin_mesh(coors,cells, local_comm,comm,colour)
tic = tm.time()
tictoc = tic-toc
print(f'rank = {rank}, time of building mesh = {tic-toc}!')

E =  FiniteElement("CG",mesh.ufl_cell(),1)
V = FunctionSpace(mesh,E)

v = TrialFunction(V)
v_ = TestFunction(V)

F = inner(grad(v),grad(v_))*dx
L = 0.1*v_*dx
v = Function(V)
bottom = CompiledSubDomain("(near(x[1],0)) && on_boundary")
bc = DirichletBC(V,Constant(1.0),bottom)
toc = tm.time()
solve(F==L,v,[bc],
      solver_parameters={'linear_solver': 'gmres',
                         'preconditioner': 'ilu'})
      #solver_parameters={"linear_solver": "lu"},
      #form_compiler_parameters={"optimize": True})
tic = tm.time()
print(f'rank = {rank}, time of solving = {tic-toc}!')
print(f'meshing is {(tic-toc)/tictoc} faster!')

encoding = XDMFFile.Encoding.HDF5 if has_hdf5() else XDMFFile.Encoding.ASCII

out = XDMFFile(MPI.comm_world, "result_MPI/v.xdmf")
out.write(v, encoding)

plot(mesh)
plt.show()
    
