from dolfin import *
import numpy as np

#this solution was suggested by Hron in
#https://bitbucket.org/fenics-project/dolfin/issues/403/mesheditor-and-meshinit-not-initializing

code="""
#include <dolfin/mesh/MeshPartitioning.h>
#include <pybind11/pybind11.h>

namespace dolfin {
void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
{
MeshPartitioning::build_distributed_mesh(*mesh);
}
};
PYBIND11_MODULE(SIGNATURE, m)
{
m.def("build_distributed_mesh", &dolfin::build_distributed_mesh);
}
"""
mod = compile_cpp_code(code)

#coords and cells have to be on root 0
def build_dolfin_mesh(coords,cls,rank):
    m = Mesh()
    if rank == 0:
        editor = MeshEditor()
        n_coords = coords.shape[0] 
        n_cls = cls.shape[0]
    
        editor.open(m,'triangle', 2, 2)  # top. and geom. dimension are both 2
        editor.init_vertices(n_coords )  # number of vertices
        editor.init_cells(n_cls)     # number of cells
        for i in range(n_coords):
            editor.add_vertex(i, coords[i])
        for i in range(n_cls):
            editor.add_cell(i, cls[i])
        editor.close()
    mod.build_distributed_mesh(m)
    return m


if __name__ =="__main__":
    #build_distributed_mesh = compile_extension_module(code).build_distributed_mesh
    comm = MPI.comm_world
    rank=comm.Get_rank()

    #create the mesh by mesheditor 
    m=Mesh()
    
    if rank==0 :
        editor = MeshEditor()
        editor.open(m,'triangle', 2, 2)
        editor.init_vertices(4)  # number of vertices
        editor.init_cells(2)     # number of cells
        editor.add_vertex(0, [0.0,0.0])
        editor.add_vertex(1, [1.0,0.0])
        editor.add_vertex(2, [1.0,1.0])
        editor.add_vertex(3, [0.0,1.0])        
        editor.add_cell(0, [0,1,2])
        editor.add_cell(1, [0,2,3])
        editor.close()
        m.init()
        m.order()
        info(m)

    mod.build_distributed_mesh(m)
    info(m)
    plot(m)
    import matplotlib.pyplot as plt
    plt.show()
