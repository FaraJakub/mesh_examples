from dolfin import * 
import numpy as np
import matplotlib.pyplot as plt

# rebuilding mesh using MeshEditor
def dolfin_mesh(coords,cls):
    n_coords = coords.shape[0] 
    n_cls = cls.shape[0]
    
    editor = MeshEditor()
    mesh = Mesh()
    editor.open(mesh,'triangle', 2, 2)  # top. and geom. dimension are both 2
    editor.init_vertices(n_coords )  # number of vertices
    editor.init_cells(n_cls)     # number of cells
    for i in range(n_coords):
        editor.add_vertex(i, coords[i])
    for i in range(n_cls):
        editor.add_cell(i, cls[i])
    editor.close()
    return mesh

mesh = UnitSquareMesh(10,10,'crossed')

#get mesh specifications
cells = mesh.cells()        #[[i1,j1,k1],[i2,j2,k2],...[in,jn,kn]]
coors = mesh.coordinates()  #[[x1,y1],[x2,y2],...,[xn,yn]]

#operations with cells and coordinates
coors[:,1] = (coors[:,1]+1)**2

#--------if no changes in cells--------------
#new_mesh = mesh
#new_mesh.coordinates()[:,:] = coors[:,:]
#--------------------------------------------

#----------if topological changes------------
cells = np.delete(cells, (0,1), axis = 0)
#for i in range(old_cells.shape[0]//2):
#    cells = np.delete(cells, (0), axis = 0)
#rebuilding mesh
new_mesh = dolfin_mesh(coors,cells)
#-------------------------------------------

#---------visualization---------------------
plot(new_mesh)
plt.show()
