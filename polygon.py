"""
This file is designed to demonstrate a projection of function
on moving mesh mesh, where is polygonial subdomain.

This is NOT working in parallel, because of the polygonial subdomain. 
"""
from dolfin import *
from mshr import *
import numpy as np
import matplotlib.pyplot as plt

def make_points_circle(S,r,max_points):
    alpha = 0.0
    points  = [Point(S[0]+r,S[1])]
    for i in range(max_points-1):
        alpha += np.pi*2/max_points
        points.insert(len(points),Point(S[0]+r*cos(alpha),S[1]+r*sin(alpha)))
        #print(S[0]+r*cos(alpha),S[1]+r*sin(alpha))
    return (points)

def move_points(points,u):
    for i in range(len(points)):
        p = points[i]
        print(u(p)[0],u(p)[1])
        points[i] = Point(p.x() + u(p)[0],p.y() + u(p)[1])
    return points

#_______________Domain and function spaces_______________________________
P1 = Point(0.0,0.0)
P2 = Point(2.0,1.0)
S = Point(0.5,0.5)
r = 0.2
num_of_points = 50
resolution = 30
points = make_points_circle(S,r,num_of_points)
Omega = Rectangle(P1,P2)
OUTER = 1
INNER = 2
try:
    Omega_i = Polygon(points)
except:
    Omega_i = Polygon(points[::-1])
Omega.set_subdomain(OUTER,Omega-Omega_i)
Omega.set_subdomain(INNER,Omega_i)
mesh = generate_mesh(Omega,resolution)

subdomains = MeshFunction("size_t", mesh, 2, mesh.domains())

E =  VectorElement("CG",mesh.ufl_cell(),1)
V = FunctionSpace(mesh,E)
#_________________________________________________________________________
#_______________Making suitable diplacement____________________________

#u_expression = Expression(('-0.4*x[0]*(x[0]-2.0)','0.0 '),degree = 2)
#u = project(u_expression,V)

u = TrialFunction(V)
u_ = TestFunction(V)
bndry = CompiledSubDomain("(near( x[1]*(x[1]-top),0.0 ) || near( x[0]*(x[0]-right) ,0.0))",
                             top = P2.y(),right = P2.x())

NoSlip = Constant((0.0,0.0))
bc_walls = DirichletBC(V,NoSlip,bndry)
bcs = [bc_walls]
dx = Measure("dx")(domain = mesh,subdomain_data=subdomains) 
g = Constant((1.,0.))

F = 2*inner(grad(u),grad(u_))*dx(INNER) + 4*inner(grad(u),grad(u_))*dx(OUTER)
L = 4*inner(g,u_)*dx(INNER) + 8*inner(g,u_)*dx(OUTER) 

u = Function(V)
solve(F ==L,u,bcs)

#_________________________________________________________________________
#________________Making the new mesh______________________________________
new_mesh = Mesh(mesh)
ALE.move(new_mesh,u)
#__________________________________________________________________________
#Function to be projected
v_expression = Expression(('x[0]','x[1] '),degree = 2)
v = Function(V)
v = project(v_expression,V)

encoding = XDMFFile.Encoding.HDF5 if has_hdf5() else XDMFFile.Encoding.ASCII

out = XDMFFile(MPI.comm_world, "result_v/v.xdmf")
out.write(v, encoding)


E_new =  VectorElement("CG",new_mesh.ufl_cell(),1)
V_new = FunctionSpace(new_mesh,E_new)
v_new = Function(V_new)
v_new.vector()[:] = v.vector()[:]


out = XDMFFile(MPI.comm_world, "result_v/v_new.xdmf")
out.write(v_new, encoding)

#_______________New domain and function spaces_______________________________
Omega = Rectangle(P1,P2)
points = move_points(points,u)

try:
    Omega_i = Polygon(points)
except:
    Omega_i = Polygon(points[::-1])
Omega.set_subdomain(OUTER,Omega-Omega_i)
Omega.set_subdomain(INNER,Omega_i)
mesh = generate_mesh(Omega,resolution)
plot(mesh)
plt.show()
E =  VectorElement("CG",mesh.ufl_cell(),1)
V = FunctionSpace(mesh,E)
#_________________________________________________________________________
#________Projection schifted v on V _____________________________________ 
v = Function(V)
v_new.set_allow_extrapolation(True) #necessery in parallel
v = interpolate(v_new,V)

out = XDMFFile(MPI.comm_world, "result_v/v_.xdmf")
out.write(v, encoding)
#__________________________________________________________________________
