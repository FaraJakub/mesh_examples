import dolfin as df
import mshr as ms
import numpy as np
import matplotlib.pyplot as plt

#_______________Domain and function spaces_______________________________
P1 = df.Point(0.0,0.0)
P2 = df.Point(2.0,1.0)
S = df.Point(0.5,0.5)
r = 0.2
domain = ms.Rectangle(P1,P2)-ms.Circle(S,r)
resolution = 25
mesh = ms.generate_mesh(domain,resolution)

E =  df.VectorElement("CG",mesh.ufl_cell(),1)
V = df.FunctionSpace(mesh,E)
#_________________________________________________________________________
#_______________Making suitable diplacement____________________________

#u_expression = df.Expression(('-0.2*x[0]*(x[0]-2.0)','0.0 '),degree = 2)
#u = df.project(u_expression,V)

u = df.TrialFunction(V)
u_ = df.TestFunction(V)
circle_bndry =  df.CompiledSubDomain("pow((x[0]-s0),2)+ pow((x[1]-s1),2)-r_sqrt<eps",
                                     s0 = S[0],s1 = S[1],r_sqrt = r**2,eps = 0.0001)
bndry = df.CompiledSubDomain("(near( x[1]*(x[1]-top),0.0 ) || near( x[0]*(x[0]-right) ,0.0))",
                             top = P2.y(),right = P2.x())

C = df.Constant((0.2,0.0))
NoSlip = df.Constant((0.0,0.0))
bc_circle = df.DirichletBC(V,C,circle_bndry)
bc_walls = df.DirichletBC(V,NoSlip,bndry)
bcs = [bc_circle,bc_walls]
F = df.inner(df.grad(u),df.grad(u_))*df.dx
L = df.inner(NoSlip,u_)*df.dx

u = df.Function(V)
df.solve(F ==L,u,bcs)
#_________________________________________________________________________
#________________Making the new mesh______________________________________
new_mesh = df.Mesh(mesh)
df.ALE.move(new_mesh,u)
# df.plot(mesh)
# plt.show()
# df.plot(new_mesh)
# plt.show()
#__________________________________________________________________________
#Function to be projected
v_expression = df.Expression(('x[0]','x[1] '),degree = 2)
v = df.Function(V)
v = df.project(v_expression,V)

encoding = df.XDMFFile.Encoding.HDF5 if df.has_hdf5() else df.XDMFFile.Encoding.ASCII

out = df.XDMFFile(df.MPI.comm_world, "result_v/v.xdmf")
out.write(v, encoding)


E_new =  df.VectorElement("CG",new_mesh.ufl_cell(),1)
V_new = df.FunctionSpace(new_mesh,E_new)
v_new = df.Function(V_new)
v_new.vector()[:] = v.vector()[:]


out = df.XDMFFile(df.MPI.comm_world, "result_v/v_new.xdmf")
out.write(v_new, encoding)

#_______________New domain and function spaces_______________________________
P1_ = df.Point(0.0,0.0)
P2_ = df.Point(2.0,1.0)
S_ = df.Point(0.5 + 0.2 ,0.5)
r_ = 0.2
domain_ = ms.Rectangle(P1_,P2_)-ms.Circle(S_,r_)
resolution_ = 25
mesh_ = ms.generate_mesh(domain_,resolution_)

E_ =  df.VectorElement("CG",mesh_.ufl_cell(),1)
V_ = df.FunctionSpace(mesh_,E_)
#_________________________________________________________________________
#________Projection schifted v on V_ _____________________________________ 
v_ = df.Function(V_)
v_new.set_allow_extrapolation(True) #necessery in parallel
v_ = df.interpolate(v_new,V_)

out = df.XDMFFile(df.MPI.comm_world, "result_v/v_.xdmf")
out.write(v_, encoding)
#__________________________________________________________________________
